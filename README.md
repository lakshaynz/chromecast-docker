## replace 192.168.1.1 with ip address of the chromecast device
## replace https://dash1.lak.nz/ with the website you want to show on the screen
- `docker run --rm -e ARGUMENTS='-d 192.168.1.1 cast_site https://dash1.lak.nz/' ryanbarrett/catt-chromecast`

# Thanks to https://hub.docker.com/r/ryanbarrett/catt-chromecast
